echo "### Provisioning bootstrap/gce-jenkins-swarm.ps1 ###"

# Ensure C: partition is expanded
Expand-Partition

# Set timezone
$timeZone = Get-GoogleMetadataAttribute -Name timezone
if ($timeZone) {
  Set-TimeZone -Name "$timeZone"
}

#################
# Set Up Defaults
#################
$vEthernetMtu = Get-GoogleMetadataAttribute -Name vEthernetMtu -Default 1460
$dockerConfigPath = "$env:ProgramData\Docker\config"
$daemonConfigPath = $dockerConfigPath + "\daemon.json"
$daemonConfig = Get-GoogleMetadataAttribute -Name docker_daemon_config -Default (ConvertFrom-Json -InputObject "{
  `"mtu`": $vEthernetMtu,
  `"metrics-addr`" : `"0.0.0.0:7766`",
  `"experimental`" : true
}")
$daemonJson = ConvertTo-Json -InputObject $daemonConfig

# Update the Docker Daemon Config
New-Item -Path "$dockerConfigPath" -ItemType "directory" -ErrorAction SilentlyContinue > $null
Set-Content -Path "$daemonConfigPath.tmp" -Value "$daemonJson" > $null
if ((Get-FileHash -Path "$daemonConfigPath" -ErrorAction SilentlyContinue).hash -ne (Get-FileHash -Path "$daemonConfigPath.tmp").hash) {
  echo "Updating Docker daemon.json"
  Set-Content -Path "$daemonConfigPath" -Value "$daemonJson" > $null
}
Remove-Item -Path "$daemonConfigPath.tmp" > $null

# Start Docker
echo "Starting Docker"
Start-Service Docker > $null

# Install Cloudflare CA certificate (SYS-1878)

$cloudflareRootCAUrl = "https://developers.cloudflare.com/ssl/static/origin_ca_rsa_root.pem"
$cloudflareRootCAFile = "${env:TEMP}\origin_ca_rsa_root.pem"

# Download CA certificate
Invoke-WebRequest -Uri $cloudflareRootCAUrl -OutFile $cloudflareRootCAFile

# Import CA certificate into Java system-wide keystore
& "${env:JAVA_HOME}\bin\keytool.exe" -noprompt -import -v -trustcacerts -alias cloudflare-root-ca -file $cloudflareRootCAFile -keystore ${ENV:JAVA_HOME}\lib\security\cacerts -storepass changeit

# Import CA certificate into Windows root cert trust
Import-Certificate -FilePath $cloudflareRootCAFile -CertStoreLocation Cert:\LocalMachine\Root

Install-JenkinsSwarmAgent

echo "Starting Jenkins Swarm"
nssm start "Jenkins Swarm"
