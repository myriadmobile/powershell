# FC workloads need to operate in Central timezone
Write-Output "Setting timezone"
Set-TimeZone -Id "Central Standard Time"

(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/helpers/getmodules.ps1") | Invoke-Expression

# Workaround for https://github.com/kubernetes-csi/csi-driver-smb/issues/219
Get-SMBGlobalMapping | Remove-SMBGlobalMapping -Force
