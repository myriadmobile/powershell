echo "### Provisioning bootstrap/gce-docker-swarm.ps1 ###"

(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/helpers/gce.psm1") | iex

#################
# Set up defaults
#################

$joinToken = Get-GoogleMetadataAttribute -Name docker_swarm_join_token
$joinHost = Get-GoogleMetadataAttribute -Name docker_swarm_join_host
$joinAvailability = Get-GoogleMetadataAttribute -Name docker_swarm_join_availability -Default active
$ingressMTU = Get-GoogleMetadataAttribute -Name docker_swarm_ingress_mtu -Default 1460
$ingressSubnet = Get-GoogleMetadataAttribute -Name docker_swarm_ingress_subnet -Default 10.52.64.0/18
$gatewayIfIndex = (Get-NetIPConfiguration | Foreach IPv4DefaultGateway).ifIndex
$gatewayIfAddr = (Get-NetIpAddress -InterfaceIndex $gatewayIfIndex).IPAddress
$swarmActive = $((docker info).contains("Swarm: active"))

##################
# Init the cluster
##################

if (!$joinToken) {
  if(!$swarmActive){
    echo "Initializing Docker Swarm on '$gatewayIfAddr'"
    docker swarm init --advertise-addr $gatewayIfAddr --listen-addr $gatewayIfAddr`:2377 > $null
  }

  if ((docker network inspect ingress | ConvertFrom-Json).Options.'com.docker.network.mtu' -ne $ingressMTU) {
    echo "Updating Docker Swarm 'ingress' network MTU=$ingressMTU"
    'Y' | docker network rm ingress > $null
    docker network create --driver overlay --ingress --subnet=$ingressSubnet --opt com.docker.network.mtu=$ingressMTU ingress > $null
  }
}

##########################
# Join an existing cluster
##########################

if ($joinToken -and $joinHost) {
  echo "Joining Docker Swarm on '$joinHost'"
  docker swarm join --token $joinToken --availability $joinAvailability --advertise-addr $gatewayIfAddr --listen-addr $gatewayIfAddr`:2377 $joinHost > $null
}
