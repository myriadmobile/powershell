echo "### Provisioning bootstrap/gce-docker.ps1 ###"

# Ensure C: partition is expanded
Expand-Partition

#################
# Set Up Defaults
#################
$vEthernetMtu = Get-GoogleMetadataAttribute -Name vEthernetMtu -Default 1460
$dockerConfigPath = "$env:ProgramData\Docker\config"
$daemonConfigPath = $dockerConfigPath + "\daemon.json"
$daemonConfig = Get-GoogleMetadataAttribute -Name docker_daemon_config -Default (ConvertFrom-Json -InputObject "{
  `"mtu`": $vEthernetMtu,
  `"metrics-addr`" : `"0.0.0.0:7766`",
  `"experimental`" : true
}")
$daemonJson = ConvertTo-Json -InputObject $daemonConfig

# Update the Docker Daemon Config
New-Item -Path "$dockerConfigPath" -ItemType "directory" -ErrorAction SilentlyContinue > $null
Set-Content -Path "$daemonConfigPath.tmp" -Value "$daemonJson" > $null
if ((Get-FileHash -Path "$daemonConfigPath" -ErrorAction SilentlyContinue).hash  -ne (Get-FileHash -Path "$daemonConfigPath.tmp").hash) {
  echo "Updating Docker daemon.json"
  Set-Content -Path "$daemonConfigPath" -Value "$daemonJson" > $null
}
Remove-Item -Path "$daemonConfigPath.tmp" > $null

# Start Docker
echo "Starting Docker"
Start-Service Docker > $null

# Register workarounds for GCE issues
Invoke-GCEWorkarounds $vEthernetMtu
