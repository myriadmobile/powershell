# Powershell Scripts

This project contains a random assortment of useful Powershell scripts.

# Helpers

## helpers/getmodules.ps1

Downloads, installs and imports all helper modules into standard Powershell modules path

### Usage:
```powershell
(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/helpers/getmodules.ps1") | iex
```

## helpers/gce.psm1

Provides helpers for Google Compute Engine

### Usage:

```powershell
(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/helpers/gce.psm1") | iex
```

### Functions

```powershell
Get-GoogleMetadata($Name)
Get-GoogleMetadataAttribute($Name, $Default = $null)
Invoke-EnsureGoogleMetadataRoute()
Register-GoogleMetadataRouteTasks($TaskName = "EnsureGoogleMetadataRoute")
```

## helpers/java.psm1

Provides helpers for managing Oracle Java installations

### Usage:

```powershell
(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/helpers/java.psm1") | iex
```

### Functions

```powershell
Install-Java($Url = 'http://download.oracle.com/otn-pub/java/jdk/8u162-b12/0da788060d494f5095bf8624735fa2f1/jre-8u162-windows-x64.exe')

```

## helpers/system.psm1

Provides general system utilities

### Usage:

```powershell
(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/helpers/system.psm1") | iex
```

### Functions

```powershell
Add-LocalAdministrator($Name, $Password)
Install-Choco($Url = 'https://chocolatey.org/install.ps1')
Mount-NetDrive($Name, $Root, $Username, $Password)
Expand-Partition($DriveLetter)
Register-MonitorMTU($InterfaceStartsWith, $MTU, $TaskName = "MonitorMTU")
Register-MonitorHyperVSwitchMTUs($MTU, $TaskName = "MonitorHyperVSwitchMTUs", $MatchRegex = "Container NIC [0-9a-f]{8}", $ScriptRoot="C:")
```

# Bootstrap

## bootstrap/gce-docker.ps1

Bootstrap Docker on a Windows Server instance on Google Compute Engine

### Usage:

```powershell
(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/bootstrap/gce-docker.ps1") | iex
```

### GCE Metadata

| Name                  | Default                                                                 |
|-----------------------|-------------------------------------------------------------------------|
| docker_daemon_config  | { "mtu": 1460, "metrics-addr" : "0.0.0.0:7766", "experimental" : true } |

## bootstrap/gce-docker-swarm.ps1

Bootstrap Docker Swarm on a Windows Server instance on Google Compute Engine

### Usage:

```powershell
(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/bootstrap/gce-docker-swarm.ps1") | iex
```

### GCE Metadata

| Name                           | Default       |
|--------------------------------|---------------|
| docker_swarm_join_token        |               |
| docker_swarm_join_host         |               |
| docker_swarm_join_availability | active        |
| docker_swarm_ingress_mtu       | 1460          |
| docker_swarm_ingress_subnet    | 10.52.64.0/18 |


## bootstrap/gce-jenkins-swarm.ps1

Bootstrap a Jenkins Swarm slave on Google Compute Engine

### Usage:

```powershell
(new-object Net.WebClient).DownloadString("https://bitbucket.org/myriadmobile/powershell/raw/HEAD/bootstrap/gce-jenkins-swarm.ps1") | iex
```

### GCE Metadata

| Name           | Default                                                                                                          |
|----------------|------------------------------------------------------------------------------------------------------------------|
| java_url       | http://download.oracle.com/otn-pub/java/jdk/8u162-b12/0da788060d494f5095bf8624735fa2f1/jre-8u162-windows-x64.exe |
| jenkins_path   | C:\jenkins                                                                                                       |
| swarm_url      | https://repo.jenkins-ci.org/releases/org/jenkins-ci/plugins/swarm-client/3.8/swarm-client-3.8.jar                |
| swarm_labels   | swarm windows                                                                                                    |
| swarm_master   | https://jenkins                                                                                                  |
| swarm_name     | windows                                                                                                          |
| swarm_password | jenkins                                                                                                          |
| swarm_username | jenkins                                                                                                          |
