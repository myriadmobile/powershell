Function Add-LocalAdministrator($Name, $Password) {
  Write-Host "Adding new local user '$Name' as an Administrator"
  New-LocalUser -Name "$Name" -Password (ConvertTo-SecureString -AsPlainText -Force -String "$Password") > $null
  Add-LocalGroupMember -Group "Administrators" -Member "$Name" > $null
}

Function Install-Choco($Url = 'https://chocolatey.org/install.ps1') {
  $installationDir = "$env:ProgramData\chocolatey"
  If (!(test-path $installationDir)) {
    Write-Host "Installing Chocolatey"
    Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString($Url)) > $null
  }
}

Function Restart-ComputerSoon($seconds = 3) {
  Write-Host "Restarting computer in $seconds seconds"
  Write-Host "------------------------------------------------------------"
  shutdown /r /t $seconds
  Exit 0
}

Function Stop-ComputerSoon($seconds = 3) {
  Write-Host "Shutting down computer in $seconds seconds"
  Write-Host "------------------------------------------------------------"
  shutdown /s /t $seconds /d p:2:4 /f
}

Function Install-DockerIfNotInstalled() {
  if ((Get-WindowsFeature Containers).Installed) {
    Write-Host  "Windows Containers is already installed"
  } else {
    Write-Host "Installing Windows Containers"
    $featureInstall = Add-WindowsFeature Containers

    if ($featureInstall.RestartNeeded -eq "Yes") {
      Restart-ComputerSoon
    }
  }

  Write-Host "Installing Docker"
  cd $env:temp
  Invoke-WebRequest -UseBasicParsing "https://raw.githubusercontent.com/microsoft/Windows-Containers/Main/helpful_tools/Install-DockerCE/install-docker-ce.ps1" -OutFile install-docker-ce.ps1
  .\install-docker-ce.ps1
}

Function Disable-WindowsFirewall() {
  $firewallProfiles = Get-NetFirewallProfile
  Foreach ($firewallProfile in $firewallProfiles) {
    if ($firewallProfile.Enabled) {
      $profileName = $firewallProfile.Name
      Write-Host "Disabling Windows Firewall for $profileName profile"
      Set-NetFirewallProfile -Profile $profileName -Enabled False
    }
  }
}

Function Disable-WindowsUpdate() {
  # https://docs.microsoft.com/en-us/windows-server/administration/server-core/server-core-servicing
  Write-Host "Disabling automatic Windows updates"
  net stop wuauserv 2>&1 > $null
  c:\windows\system32\Cscript c:\windows\system32\scregedit.wsf /AU 1 > $null
}

Function Remove-WindowsDefender() {
  $features = (Get-WindowsFeature *defender*)
  Foreach ($feature in $features) {
    if ($feature.Installed) {
      $featureName = $feature.Name
      Write-Host "Removing $featureName"
      $feature | Remove-WindowsFeature > $null
    }
  }
}

Function Mount-NetDrive($Name, $Root, $Username, $Password) {
  Write-Host "Mounting network drive '$Root' to '$Name'"
  $secpassword = ConvertTo-SecureString -String "$Password" -AsPlainText -Force
  $mountCreds = New-Object System.Management.Automation.PSCredential ($Username, $secpassword)
  New-PSDrive -Persist -Name "$Name" -PSProvider "FileSystem" -Root "$Root" -Credential $mountCreds -Scope Global > $null
}

Function Expand-Partition($DriveLetter = 'C') {
  $currentSize = (Get-Partition -DriveLetter $DriveLetter).size
  $maxSize = (Get-PartitionSupportedSize -DriveLetter $DriveLetter).sizeMax
  if ($currentSize -ne $maxSize) {
    Write-Host "Expanding $DriveLetter`: Partition"
    Resize-Partition -DriveLetter $DriveLetter -Size $maxSize
  }
}

Function Register-MonitorMTU($InterfaceStartsWith, $MTU, $TaskName = "MonitorMTU") {
  # create the command for actually monitoring and setting the mtu
  $command = "Get-NetIPInterface -IncludeAllCompartments | % {
    if (`$_.InterfaceAlias.StartsWith(`"$InterfaceStartsWith`") -and (`$_.NlMtu -ne $MTU)) {
      Write-Host `"Setting MTU on `$(`$_.InterfaceAlias) to $MTU`"
      Set-NetIPInterface -IncludeAllCompartments -InterfaceIndex `$_.InterfaceIndex -NlMtuBytes $MTU
    }
  }"
  # encode the command to avoid any issues with escaping or special characters
  $encodedCommand = [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($command))

  # due to a bug in the Windows scheduler, repeating tasks that start are boot do not work
  # so we'll create a task that starts at boot that creates and the actual monitoring task
  $monitorCommand = "
    `$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument '-ExecutionPolicy Bypass -NonInteractive -NoProfile -EncodedCommand `"$encodedCommand`"'
    `$trigger = New-ScheduledTaskTrigger -Once -At (Get-Date) -RepetitionInterval (New-TimeSpan -Minutes 1)
    `$principal = New-ScheduledTaskPrincipal -UserID 'NT AUTHORITY\SYSTEM' -LogonType ServiceAccount -RunLevel Highest
    Unregister-ScheduledTask -TaskName '$TaskName' -Confirm:`$false -ErrorAction SilentlyContinue
    Register-ScheduledTask -TaskName '$TaskName' -Action `$action -Trigger `$trigger -Principal `$principal
    Start-ScheduledTask -TaskName '$TaskName'
  "
  $encodedMonitorCommand = [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($monitorCommand))
  $action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-ExecutionPolicy Bypass -NonInteractive -NoProfile -EncodedCommand `"$encodedMonitorCommand`""
  $trigger = New-ScheduledTaskTrigger -AtStartup
  $principal = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
  Unregister-ScheduledTask -TaskName "$($TaskName)Scheduler" -Confirm:$false -ErrorAction SilentlyContinue > $null
  Register-ScheduledTask -TaskName "$($TaskName)Scheduler" -Action $action -Trigger $trigger -Principal $principal > $null
  Start-ScheduledTask -TaskName "$($TaskName)Scheduler" > $null

  Write-Host "Exec Register-MonitorMTU(InterfaceStartsWith = '$InterfaceStartsWith', MTU = '$MTU', TaskName = '$TaskName')"
}

Function Register-MonitorHyperVSwitchMTUs($MTU, $TaskName = "MonitorHyperVSwitchMTUs", $MatchRegex = "Container NIC [0-9a-f]{8}|Ethernet", $ScriptRoot = "C:") {
  $scriptPath = "$ScriptRoot\On$TaskName.ps1"
  $command = "# Responds to Hyper-V-VmSwitch Event 7 to change NIC MTU
  param(`$eventRecordID,`$eventChannel)
  `$event = Get-WinEvent -LogName `"`$eventChannel`" -FilterXPath `"<QueryList><Query Id='0' Path='`$eventChannel'><Select Path='`$eventChannel'>*[System[(EventRecordID=`$eventRecordID)]]</Select></Query></QueryList>`"
  if(`$event.Message -match `"$MatchRegex`") {
    `$nic = `"vEthernet (`$(`$matches[0]))`"
    Set-NetIPInterface -IncludeAllCompartments -InterfaceAlias `$nic -NlMtuBytes $MTU
  }"
  $command | Set-Content -Path "$scriptPath"
  $task = "<?xml version=`"1.0`" encoding=`"UTF-16`"?>
  <Task version=`"1.2`" xmlns=`"http://schemas.microsoft.com/windows/2004/02/mit/task`">
    <RegistrationInfo>
      <Date>2018-02-02T08:00:23.7868695</Date>
      <Author>NT AUTHORITY\SYSTEM</Author>
      <URI>Event Viewer Tasks\Docker MTU Watcher</URI>
    </RegistrationInfo>
    <Triggers>
      <EventTrigger>
        <Enabled>true</Enabled>
        <Subscription>&lt;QueryList&gt;&lt;Query Id=`"0`" Path=`"System`"&gt;&lt;Select Path=`"System`"&gt;*[System[Provider[@Name=`'Microsoft-Windows-Hyper-V-VmSwitch`'] and EventID=7]]&lt;/Select&gt;&lt;/Query&gt;&lt;/QueryList&gt;</Subscription>
        <ValueQueries>
          <Value name=`"eventChannel`">Event/System/Channel</Value>
          <Value name=`"eventRecordID`">Event/System/EventRecordID</Value>
        </ValueQueries>
    </EventTrigger>
    </Triggers>
    <Principals>
      <Principal id=`"Author`">
        <UserId>S-1-5-18</UserId>
        <RunLevel>HighestAvailable</RunLevel>
      </Principal>
    </Principals>
    <Settings>
      <MultipleInstancesPolicy>Parallel</MultipleInstancesPolicy>
      <DisallowStartIfOnBatteries>false</DisallowStartIfOnBatteries>
      <StopIfGoingOnBatteries>false</StopIfGoingOnBatteries>
      <AllowHardTerminate>true</AllowHardTerminate>
      <StartWhenAvailable>false</StartWhenAvailable>
      <RunOnlyIfNetworkAvailable>false</RunOnlyIfNetworkAvailable>
      <IdleSettings>
        <StopOnIdleEnd>true</StopOnIdleEnd>
        <RestartOnIdle>false</RestartOnIdle>
      </IdleSettings>
      <AllowStartOnDemand>true</AllowStartOnDemand>
      <Enabled>true</Enabled>
      <Hidden>false</Hidden>
      <RunOnlyIfIdle>false</RunOnlyIfIdle>
      <WakeToRun>false</WakeToRun>
      <ExecutionTimeLimit>PT72H</ExecutionTimeLimit>
      <Priority>7</Priority>
    </Settings>
    <Actions Context=`"Author`">
      <Exec>
        <Command>powershell.exe</Command>
        <Arguments>$scriptPath -ExecutionPolicy Bypass -NonInteractive -NoProfile -eventRecordID `$(eventRecordID) -eventChannel `$(eventChannel)</Arguments>
      </Exec>
    </Actions>
  </Task>"
  Unregister-ScheduledTask -TaskName "$TaskName" -Confirm:$false -ErrorAction SilentlyContinue > $null
  Register-ScheduledTask -Xml "$task" -TaskName "$TaskName" > $null

  Write-Host "Exec Register-MonitorHyperVSwitchMTUs(MTU = '$MTU', TaskName = '$TaskName', MatchRegex = $MatchRegex, ScriptPath = $ScriptPath)"
}

# https://blogs.endjin.com/2014/07/how-to-retry-commands-in-powershell/
function Use-RetryCommand {
  param (
    [Parameter(Mandatory = $true)][string]$command,
    [Parameter(Mandatory = $true)][hashtable]$args,
    [Parameter(Mandatory = $false)][int]$retries = 5,
    [Parameter(Mandatory = $false)][int]$secondsDelay = 2
  )

  # Setting ErrorAction to Stop is important. This ensures any errors that occur in the command are
  # treated as terminating errors, and will be caught by the catch block.
  $args.ErrorAction = "Stop"

  $retrycount = 0
  $completed = $false

  while (-not $completed) {
    try {
      & $command @args
      Write-Verbose ("Command [{0}] succeeded." -f $command)
      $completed = $true
    }
    catch {
      if ($retrycount -ge $retries) {
        Write-Verbose ("Command [{0}] failed the maximum number of {1} times." -f $command, $retrycount)
        throw
      }
      else {
        Write-Verbose ("Command [{0}] failed. Retrying in {1} seconds." -f $command, $secondsDelay)
        Start-Sleep $secondsDelay
        $retrycount++
      }
    }
  }
}

Function Install-PowershellYaml {
  Set-PowershellCrypto
  $version = "0.4.12"
  $destinationPath = "C:\Program Files\WindowsPowerShell\Modules\powershell-yaml\${version}"

  if (!(Test-Path -Path $destinationPath)) {
    Write-Host "Installing powershell-yaml"
    $sourceUrl = "https://github.com/cloudbase/powershell-yaml/archive/refs/tags/v${version}.zip"
    $zipFile = "C:\Windows\Temp\powershell-yaml.zip"

    mkdir $destinationPath -Force > $null
    (new-object Net.WebClient).DownloadFile($sourceUrl, $zipFile)
    Expand-Archive $zipFile -DestinationPath $destinationPath

    $subDir = (ls $destinationPath).Name
    mv $destinationPath\$subDir\* $destinationPath
    Remove-Item -Recurse -Force $destinationPath\$subDir
  }
}

Function Install-PsAke {
  $version = "4.9.0"
  Add-Type -assembly "System.IO.Compression.Filesystem"

  $psakeDownloadUrl = "https://github.com/psake/psake/archive/v${version}.zip"

  If (!(test-path "C:\PSModules\psake")) {
    # Create C:\PSModules directory
    [System.Io.Directory]::CreateDirectory("C:\PSModules") > $null

    # Download and extract psake zip file
    (New-Object Net.WebClient).DownloadFile($psakeDownloadUrl, "C:\PSModules\psake.zip")
    [IO.Compression.Zipfile]::ExtractToDirectory("C:\PSModules\psake.zip", "C:\PSModules\psake_temp")
    [IO.File]::Delete("C:\PSModules\psake.zip")
    [System.IO.Directory]::Move("C:\PSModules\psake_temp\psake-$version\src", "C:\PSModules\psake")
    [IO.Directory]::Delete("C:\PSModules\psake_temp", $true)

    # Add C:\PSModules to `PSModulePath`, so within powershell you can `Import-Module psake`
    $env:PSModulePath = $env:PSModulePath + ";C:\PSModules"
    [Environment]::SetEnvironmentVariable("PSModulePath", $env:PSModulePath, [EnvironmentVariableTarget]::Machine)

    # Add extracted src directory to path, so psake is available as a command
    $env:PATH = $env:PATH + ";C:\PSModules\psake\"
    [Environment]::SetEnvironmentVariable("PATH", $env:PATH, [EnvironmentVariableTarget]::Machine)
  }
}

Function Install-Golang {
  param(
    $Version = "1.24.0"
  )

  Write-Host "Installing golang $Version"

  $installedPath = "C:\Program Files\Go\bin\"
  choco install -y golang --force --version $Version > $null
  if($?) {
    # Add golang to system path
    $env:PATH = $env:PATH + ";" + $installedPath
    [Environment]::SetEnvironmentVariable("PATH", $env:PATH, [EnvironmentVariableTarget]::Machine)

    if(Get-Command "go" -ErrorAction SilentlyContinue) {
      Write-Host "golang $Version installed successfully"
      return $true
    } else {
      return $false
    }
  } else {
    return $false
  }
}

Function Install-SevenZip {
  $installerUrl = "https://github.com/ip7z/7zip/releases/download/24.09/7z2409-x64.msi"
  $installerSha256 = "ec6af1ea0367d16dde6639a89a080a524cebc4d4bedfe00ed0cac4b865a918d8"
  $installationDir = "C:\Program Files\7-Zip"

  If (!(test-path $installationDir)) {
    (new-object Net.WebClient).DownloadFile($installerUrl, "C:\Windows\Temp\7zip-installer.msi")
    $sevenZipInstallerHash = (Get-FileHash C:\Windows\Temp\7zip-installer.msi -Algorithm SHA256).Hash.ToLower()
    If ( $installerSha256 -eq $sevenZipInstallerHash ) {
      Write-Host "Installing 7-Zip"
      C:\Windows\Temp\7zip-installer.msi /q

      $tries = 0
      While (!(Test-Path "$installationDir\7z.exe")) {
        $tries++
        If ($tries -gt 20) {
          Throw "$installationDir\7z.exe not found"
        }
        Start-Sleep -m 500
      }
      # Add 7z to path and reload
      $path = [Environment]::GetEnvironmentVariable('Path', 'Machine')
      $newPathEntry = ";" + $installationDir
      $newPath = $path + $newPathEntry
      [Environment]::SetEnvironmentVariable('Path', $newPath, 'Machine')
      $env:Path = [Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [Environment]::GetEnvironmentVariable("Path", "User")
    }
    Else {
      Throw "SHA256 of 7-zip installer doesn't match what's expected"
    }
  }
}

Function Set-PowershellCrypto {
  Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto' -Value '1' -Type DWord
  [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
}

Function Install-Nssm {
  If ($null -eq (Get-Command "nssm.exe" -ErrorAction SilentlyContinue)) {
    Install-Choco
    Write-Host "Installing NSSM"
    choco install nssm -y > $null
  }
}

Function Install-Git {
  If ($null -eq (Get-Command "git.exe" -ErrorAction SilentlyContinue)) {
    Install-Choco
    Write-Host "Installing git"
    choco install git -y > $null
    $fullGitBinPath = "C:\Program Files\Git\bin\"
    $newMachinePath = [System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";$fullGitBinPath"
    [Environment]::SetEnvironmentVariable("PATH", $newMachinePath, [EnvironmentVariableTarget]::Machine)
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path", "Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path", "User")
  }
}

Function New-MMSmbGlobalMapping($NetworkPath, $DriveLetter, $Username, $Password) {
  $DriveLetterFull = "${DriveLetter}:"

  $ExistingMappings = Get-SmbGlobalMapping
  Foreach ($em in $ExistingMappings) {
    if (($em.LocalPath -Eq $DriveLetterFull) -And ($em.RemotePath -Eq $NetworkPath)) {
      Write-Host "$NetworkPath already mapped as $DriveLetterFull"
      return
    }
  }

  $PasswordSecure = ConvertTo-SecureString -String $Password -AsPlainText -Force
  $Credential = New-Object System.Management.Automation.PSCredential($Username, $PasswordSecure)
  New-SmbGlobalMapping -RemotePath $NetworkPath -Credential $Credential -LocalPath $DriveLetterFull
}

Function Use-CreateAllSmbGlobalDrives($MetadataAttribute = "samba-global-mappings") {
  $sgmBase64 = Get-GoogleMetadataAttribute -Name $MetadataAttribute
  if ($sgmBase64) {
    Write-Host "Creating smb global mappings for entries in $MetadataAttribute"
    $sgmJson = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($sgmBase64))
    $SambaGlobalMappings = ConvertFrom-Json $sgmJson
    Foreach ($mapping in $SambaGlobalMappings.mappings) {
      New-MMSmbGlobalMapping -NetworkPath $mapping.network_path -DriveLetter $mapping.drive_letter -Username $mapping.username -Password $mapping.password
    }
  }
  else {
    Write-Host "No smb global mapping entries present in $MetadataAttribute"
  }
}

Function Install-GcrCredentialHelper {

  $outputFile = "C:\Windows\System32\docker-credential-gcr.exe"

  if (-not (Test-Path -Path $outputFile)) {
    Install-SevenZip
    
    $version = "2.1.26"
    Write-Host "Installing docker-credential-gcr.exe version $version"
    $url = "https://github.com/GoogleCloudPlatform/docker-credential-gcr/releases/download/v${version}/docker-credential-gcr_windows_amd64-${version}.tar.gz"

    $tempFile = "C:\Windows\Temp\gcr.tar.gz"
    (new-object Net.WebClient).DownloadFile($url, $tempFile)

    $wd = Get-Location
    Set-Location "C:\Windows\Temp"
    7z x gcr.tar.gz
    7z x gcr.tar
    Move-Item -Path "docker-credential-gcr.exe" -Destination $outputFile
    Set-Location "${wd}"
  }
}

Function Write-ContainerDnsWorkaround {
  Write-Host "Writing out container DNS workarounds to c:\workarounds"
  $ips = $(kubectl -n kube-system get pods --selector=k8s-app=kube-dns -o=jsonpath='{range .items[*]}{.status.podIP} {end}')
  $ips = $ips.Trim().Split()
  $ipsCsv = [system.String]::Join(",", $ips)

  mkdir c:\workarounds -Force > $null
  Write-Host "Set-DnsClientServerAddress -InterfaceIndex (Get-NetAdapter | select-object -First 1).ifindex -ServerAddresses $ipsCsv" > c:\workarounds\container_dns.ps1
  Write-Host "Write-Host 'Disabling DNS cache'`nNew-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters' -Name MaxCacheTtl -Value 0 -Type DWord | Out-Null`nNew-ItemProperty -Path 'HKLM:\SYSTEM\CurrentControlSet\Services\Dnscache\Parameters' -Name MaxNegativeCacheTtl -Value 0 -Type DWord | Out-Null`nRestart-Service dnscache | Out-Null" > c:\workarounds\container_dns_disable_cache.ps1
}

Function Register-ContainerDnsWorkaround($TaskName = "ContainerDnsWorkaround") {
  # due to a bug in the Windows scheduler, repeating tasks that start are boot do not work
  # so we'll create a task that starts at boot that creates and the actual monitoring task
  $monitorCommand = "
    `$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument '-ExecutionPolicy Bypass -NonInteractive -NoProfile -Command `"Write-ContainerDnsWorkaround`"'
    `$trigger = New-ScheduledTaskTrigger -Once -At (Get-Date) -RepetitionInterval (New-TimeSpan -Minutes 1)
    `$principal = New-ScheduledTaskPrincipal -UserID 'NT AUTHORITY\SYSTEM' -LogonType ServiceAccount -RunLevel Highest
    Unregister-ScheduledTask -TaskName '$TaskName' -Confirm:`$false -ErrorAction SilentlyContinue
    Register-ScheduledTask -TaskName '$TaskName' -Action `$action -Trigger `$trigger -Principal `$principal
    Start-ScheduledTask -TaskName '$TaskName'
  "
  $encodedMonitorCommand = [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($monitorCommand))
  $action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-ExecutionPolicy Bypass -NonInteractive -NoProfile -EncodedCommand `"$encodedMonitorCommand`""
  $trigger = New-ScheduledTaskTrigger -AtStartup
  $principal = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
  Unregister-ScheduledTask -TaskName "$($TaskName)Scheduler" -Confirm:$false -ErrorAction SilentlyContinue > $null
  Register-ScheduledTask -TaskName "$($TaskName)Scheduler" -Action $action -Trigger $trigger -Principal $principal > $null
  Start-ScheduledTask -TaskName "$($TaskName)Scheduler" > $null

  Write-Host "Exec Register-EnsureGoogleMetadataRoute(TaskName = '$TaskName')"
}

# Naive check for orchestration platform (kubernetes, marathon, unknown) based on environment variables that should be used on either
function Get-OrchestrationPlatform {
  $platform = "unknown"
  if (Test-Path env:KUBERNETES_PORT) {
    $platform = "kubernetes"
  }
  elseif ( Test-Path env:MARATHON_APP_ID ) {
    $platform = "marathon"
  }

  return $platform
}

function Clear-AllEventLogs {
  Write-Host "Clearing event logs"
  $Logs = Get-EventLog -List | ForEach-Object { $_.Log }
  $Logs | ForEach-Object { Clear-EventLog -Log $_ }
}

function Clear-Temp {
  Write-Host "Deleting everything in C:\Windows\Temp"
  Remove-Item -Recurse -Force C:\Windows\Temp\*
}

function Install-DebugTools {
  dism /online /Enable-Feature /FeatureName:TelnetClient
  choco install notepadplusplus procexp wireshark winpcap -y
}

Function Install-JenkinsSwarmAgent() {
  Install-Java
  Install-Nssm
  Install-Git
  Install-Curl
  Install-GcrCredentialHelper

  $jenkinsPath = 'C:\jenkins'
  New-Item -Path "$jenkinsPath" -ItemType directory -Force > $null

  $swarmUrl = Get-GoogleMetadataAttribute -Name swarm_url -Default "https://jenkins.scaleticket.net/swarm/swarm-client.jar"
  $swarmPath = "$jenkinsPath\swarm.jar"

  If (!(test-path $swarmPath)) {
    Write-Host "Downloading Jenkins Swarm from '$swarmUrl' to '$swarmPath'"
    & curl.exe -L -s "$swarmUrl" --output "$swarmPath"
  }

  $command = "java.exe -jar `"$swarmPath`"" +
    " -disableClientsUniqueId" +
    " -executors 2" +
    " -fsroot `"$jenkinsPath`"" +
    " -labels `"$(Get-GoogleMetadataAttribute -Name swarm_labels -Default 'swarm windows')`"" +
    " -master `"$(Get-GoogleMetadataAttribute -Name swarm_master -Default 'https://jenkins')`"" +
    " -mode exclusive" +
    " -name `"$(Get-GoogleMetadataAttribute -Name swarm_name -Default $(Get-GoogleMetadata -Name name -Default 'windows'))`"" +
    " -password `"$(Get-GoogleMetadataAttribute -Name swarm_password -Default 'jenkins')`"" +
    " -username `"$(Get-GoogleMetadataAttribute -Name swarm_username -Default 'jenkins')`""
  $command | Set-Content "$jenkinsPath\start.bat"

  $service = "Jenkins Swarm"
  If (-Not (Get-Service "$service" -ErrorAction SilentlyContinue)) {
    echo "Installing Jenkins Swarm Service"
    nssm install "$service" "$jenkinsPath\start.bat" > $null
    nssm set "$service" AppStdout "$jenkinsPath\jenkins-swarm.log"
    nssm set "$service" AppStderr "$jenkinsPath\jenkins-swarm.err.log"
    nssm set "$service" Start SERVICE_DEMAND_START > $null
  }
}

Function Install-VSRemoteTools {
  $downloadUrl = "https://aka.ms/vs/17/release/RemoteTools.amd64ret.enu.exe"
  (new-object Net.WebClient).DownloadFile($downloadUrl, "C:\Windows\Temp\RemoteTools.exe")

  $exitCode = (Start-Process -FilePath "C:\Windows\Temp\RemoteTools.exe" -ArgumentList "/passive /norestart /quiet" -Wait -Passthru).ExitCode
  if ($ExitCode -ne 0) {
    Write-Host "Failed to install MSChart (Result=$ExitCode)"
    Exit $ExitCode
  }
  else {
    Write-Host "Installation of MSChart finished"
  }
  Start-Process -Wait -FilePath "C:\Windows\Temp\RemoteTools.exe" -ArgumentList "/quiet /norestart"
}

Function Remove-DockerImages {
  # Attempt to remove all docker images (skipping removal if $IgnoreRepositories matches the image name)
  $IgnoreRepositories = @(
    "mesos",
    "microsoft"
  )

  $Images = (docker images --format "{{json . }}") | Out-String
  $Images = $Images -split "`r`n"

  ForEach ($Image in $Images) {
    $Image = $Image | ConvertFrom-Json	
    $Ignore = $false
    ForEach ($IgnoreRepository in $IgnoreRepositories) {
      If ($Image.Repository -match $IgnoreRepository) {
        $Ignore = $true
      }
    }

    If ($Ignore) {
      continue
    }
    Else {
      $FullImageName = "$($Image.Repository):$($Image.Tag)"
      docker image rm $FullImageName 2>&1 | Out-Null
    }
  }
}

Function Register-RemoveDockerImages() {
  # Run Remove-DockerImages daily in the AM
  $action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-ExecutionPolicy Bypass -NonInteractive -Command `"Remove-DockerImages`""
 
  # Build a random time for the command to run so all instances don't run it at the same time
  $hour = Get-Random -Minimum 2 -Maximum 5
  $minute = Get-Random -Minimum 10 -Maximum 59
  $time = "$($hour):$($minute)am"

  $trigger = New-ScheduledTaskTrigger -Daily -At $time
  $principal = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
  Register-ScheduledTask -TaskName "RemoveDockerImages" -Action $action -Principal $principal -Trigger $trigger > $null

  Write-Host "Exec Register-RemoveDockerImages()"
}

Function Enable-DotNetTls12() {
  Write-Host "Enabling TLSv1.2 for .NET/OS components"
  # .net 3.5
  $defaultTlsVersions = "HKLM:\SOFTWARE\Microsoft\.NETFramework\v2.0.50727", "HKLM:\SOFTWARE\Wow6432Node\Microsoft\.NETFramework\v2.0.50727"
  foreach ($path in $defaultTlsVersions) {
    New-Item -Path $path -Force | Out-Null
    New-ItemProperty -Path $path -Name SystemDefaultTlsVersions -PropertyType DWORD -Force -Value 00000001 | Out-Null
  }

  # .net 4.6.2+
  $strongCryptoPaths = "HKLM:\SOFTWARE\Microsoft\.NETFramework\v4.0.30319", "HKLM:\SOFTWARE\Wow6432Node\Microsoft\.NETFramework\v4.0.30319"
  foreach ($path in $strongCryptoPaths) {
    New-Item -Path $path -Force | Out-Null
    New-ItemProperty -Path $path -Name SchUseStrongCrypto -PropertyType DWORD -Force -Value 00000001 | Out-Null
  }

  # OS components
  $tlsPaths = "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Client", "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\TLS 1.2\Server" 
  foreach ($path in $tlsPaths) {
    New-Item -Path $path -Force | Out-Null
    New-ItemProperty -Path $path -Name DisabledByDefault -PropertyType DWORD -Force -Value 00000000 | Out-Null
    New-ItemProperty -Path $path -Name Enabled -PropertyType DWORD -Force -Value 00000001 | Out-Null
  }
}

Function Get-WindowsVersion() {
  $VersionOverride = @{
    "21H2" = "ltsc2022"
  }
  Try {
    $Version = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion' -Name DisplayVersion -ErrorAction Stop).DisplayVersion
  }
  Catch {
    $Version = (Get-ItemProperty 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion' -Name ReleaseID -ErrorAction Stop).ReleaseID
  }
  If ($VersionOverride.ContainsKey($Version)) {
    $Version = $VersionOverride[$Version]
  }
  Return $Version
}

Function Install-Curl() {
  Install-Choco
  Write-Host "Installing curl"
  choco install curl -y > $null
}
