Function Get-GoogleMetadata($Name) {
  $headers = New-Object "System.Collections.Generic.Dictionary[[String],[String]]"
  $headers.add("Metadata-Flavor", "Google")
  Return Invoke-RestMethod -UseBasicParsing -Headers $headers -Uri http://metadata.google.internal/computeMetadata/v1/instance/$Name
}

Function Get-GoogleMetadataAttribute($Name, $Default = $null, $DecodeBase64 = $false) {
  try {
    $res = Get-GoogleMetadata attributes/$Name
	if ($DecodeBase64) {
	  $res = [System.Text.Encoding]::UTF8.GetString([System.Convert]::FromBase64String($res))
	}
    Return $res
  } catch {
    Return $Default
  }
}

Function Invoke-EnsureGoogleMetadataRoute() {
	$hostname = 'metadata.google.internal'
	$ip = '169.254.169.254'
	$connection = (Test-Connection -Count 1 -ComputerName "$hostname" -ErrorAction SilentlyContinue)
	if (!$connection) {
	  echo "Could not connect to $hostname, attempting to fix metadata route"
	  if (!(Get-Content -Path 'C:\Windows\System32\drivers\etc\hosts' | Out-String) -match "$hostname") {
			echo "Adding record for $hostname in etc/hosts"
			echo "$ip       $hostname       metadata" >> C:\Windows\System32\drivers\etc\hosts
	  }

	  if (!(Get-NetRoute -DestinationPrefix "$ip/32" -ErrorAction SilentlyContinue)) {
			echo "Adding route to $ip from default gateway"
			$gatewayIfIndex = (Get-NetIPConfiguration | Foreach IPv4DefaultGateway).ifIndex
			New-NetRoute -DestinationPrefix "$ip/32" -InterfaceIndex $gatewayIfIndex -RouteMetric 1 > $null
	  }
	} else {
	  echo "Route to $hostname is up"
	}
}

Function Invoke-FixContainerGoogleMetadataRoute() {
  $WindowsVersion = (Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion").ReleaseId
  $VersionsNeedFixing = 1709,1803

  if ($VersionsNeedFixing.Contains($WindowsVersion)) {
    route -p delete 169.254.169.254
    route add 169.254.169.254 (Get-NetRoute | Where-Object {$_.DestinationPrefix -eq "0.0.0.0/0"}).NextHop
  }
}

Function Invoke-GCEWorkarounds($vEthernetMtu = 1460) {
  netsh netkvm setparam 0 *RscIPv4 0 > $null
  Register-MonitorMTU -InterfaceStartsWith "vEthernet" -MTU "$vEthernetMtu"
  Register-MonitorHyperVSwitchMTUs -MTU "$vEthernetMtu"
  Register-GoogleMetadataRouteTasks
  Start-Sleep -Seconds 5
}

Function Register-GoogleMetadataRouteTasks($TaskName = "EnsureGoogleMetadataRoute") {
  Invoke-EnsureGoogleMetadataRoute

  # due to a bug in the Windows scheduler, repeating tasks that start are boot do not work
  # so we'll create a task that starts at boot that creates and the actual monitoring task
  $monitorCommand = "
    `$action = New-ScheduledTaskAction -Execute 'powershell.exe' -Argument '-ExecutionPolicy Bypass -NonInteractive -NoProfile -Command `"Invoke-EnsureGoogleMetadataRoute`"'
    `$trigger = New-ScheduledTaskTrigger -Once -At (Get-Date) -RepetitionInterval (New-TimeSpan -Minutes 1)
    `$principal = New-ScheduledTaskPrincipal -UserID 'NT AUTHORITY\SYSTEM' -LogonType ServiceAccount -RunLevel Highest
    Unregister-ScheduledTask -TaskName '$TaskName' -Confirm:`$false -ErrorAction SilentlyContinue
    Register-ScheduledTask -TaskName '$TaskName' -Action `$action -Trigger `$trigger -Principal `$principal
    Start-ScheduledTask -TaskName '$TaskName'
  "
  $encodedMonitorCommand = [Convert]::ToBase64String([System.Text.Encoding]::Unicode.GetBytes($monitorCommand))
  $action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-ExecutionPolicy Bypass -NonInteractive -NoProfile -EncodedCommand `"$encodedMonitorCommand`""
  $trigger = New-ScheduledTaskTrigger -AtStartup
  $principal = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount -RunLevel Highest
  Unregister-ScheduledTask -TaskName "$($TaskName)Scheduler" -Confirm:$false -ErrorAction SilentlyContinue > $null
  Register-ScheduledTask -TaskName "$($TaskName)Scheduler" -Action $action -Trigger $trigger -Principal $principal > $null
  Start-ScheduledTask -TaskName "$($TaskName)Scheduler" > $null

  echo "Exec Register-EnsureGoogleMetadataRoute(TaskName = '$TaskName')"
}
