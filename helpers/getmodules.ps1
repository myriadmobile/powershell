$ErrorActionPreference = "Stop"

$baseModuleName = "Bushel"
$baseModuleDirectory = "$env:ProgramFiles\WindowsPowerShell\Modules"
$baseUrl = "https://bitbucket.org/myriadmobile/powershell/raw/HEAD/helpers"

$modules = @(
    "gce"
    "java"
    "system"
)

$TextInfo = (Get-Culture).TextInfo

Foreach ($module in $modules) {
    $moduleName = $baseModuleName + $TextInfo.ToTitleCase($module)
    $moduleDirectory = "$baseModuleDirectory\$moduleName"
    If(!(test-path $moduleDirectory)) {
        New-Item -ItemType directory -Path $moduleDirectory > $null
    }
    $url = "$baseUrl/$module.psm1"
    $moduleFile = "$moduleDirectory/$moduleName.psm1"

    echo "Installing and importing module from $url"
    $webClient = New-Object System.Net.WebClient
    $webClient.Headers.Add("Cache-Control", "no-store, max-age=1")
    $webClient.DownloadFile($url, $moduleFile)
    Import-Module -Force $moduleFile
}
