Function Install-Java($Url = 'https://github.com/adoptium/temurin21-binaries/releases/download/jdk-21.0.6%2B7/OpenJDK21U-jre_x64_windows_hotspot_21.0.6_7.msi') {
    # From https://github.com/adoptium/temurin21-binaries/releases
    $Installed = Get-WmiObject Win32_Product | Where {$_.Name -match 'Eclipse Temurin JRE' }
    if ($Installed -eq $null) {
      Write-Output "Installing Java from '$Url'"
      $installerPath = "$env:temp\jre.msi"
      (new-object Net.WebClient).DownloadFile($Url, $installerPath)
      Start-Process -Wait -FilePath "C:\Windows\System32\msiexec.exe" -ArgumentList "/i $installerPath INSTALLLEVEL=2 /quiet"
      Remove-Item $installerPath
    }
}
